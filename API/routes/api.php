<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('students', 'StudentsController@store');//Create
Route::get('students/{student}', 'StudentsController@show');//See only one
Route::get('students', 'StudentsController@index');//See all
Route::delete('students/{student}', 'StudentsController@destroy');//delete one
Route::patch('students/{student}', 'StudentsController@update');//update


