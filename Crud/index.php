
<?php
    require_once 'PgConnection.php'
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="main.js"></script>
</head>
<body>
    <div class='row'>
        <div class='col-md-6 col-md-offset-3'>
            <div class="panel panel-default">

                <div class="panel-heading">Sign In</div>
                <div class="panel-body">

                    <form action="" method="post" autocomplete="off">
                    <?php
                    $con = new PgConnection('postgres', 'postgres', 'usuarios', 5432, 'localhost');
                    $con->connect();
                    ?>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" placeholder="tu@dominio.com"
                            class="form-control" value="email">

                        <span class="help-block"></span>
                        
                    </div>

                        <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control">

                        <span class="help-block"></span>

                    </div>
                    
                    <button type="submit" class="btn btn-default">Sign In</button>
                    <?php
                    $con->runQuery('select * from usuario');
                    $con->disconnect();
                    ?>
                    </form>

                </div>

            </div>
    </div>

</div>
</body>
</html>