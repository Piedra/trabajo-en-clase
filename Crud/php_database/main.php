<?php

require_once './MySqlConnection.php';
require_once './PgConnection.php';

$con = new PgConnection('postgres', '12345', 'lugares', 5432, 'localhost');
#$con = new MySqlConnection('root', '12345', 'lugares', 3306, '172.17.0.2');
$con->connect();

$con->runStatement("DELETE FROM lugares");
$con->runStatement(
    'INSERT INTO lugares(nombre, altura, poblacion) VALUES ($1, $2, $3)',
    ['Zarcero', 2500, 20000]
);

$result = $con->runQuery('SELECT * FROM lugares');

foreach ($result as $row) {
    echo $row['id'] . "\n";
    echo $row['nombre'] . "\n";
    echo $row['altura'] . "\n";
    echo $row['poblacion'] . "\n";
}

$con->disconnect();
